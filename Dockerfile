FROM alpine:3.21.2

ENV HUGO_VERSION 0.101.0
ADD https://github.com/gohugoio/hugo/releases/download/v$HUGO_VERSION/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz /root
WORKDIR /usr/bin
RUN tar xfz /root/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz hugo && \
	chmod +x hugo && \
	rm -f /root/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz && \
	apk add --no-cache git

CMD ["/bin/sh"]

